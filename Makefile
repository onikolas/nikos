build:
	sudo rm -rf /tmp/archiso-tmp
	sudo mkarchiso -v -w /tmp/archiso-tmp releng

run: test/disk_im.qcow2
	qemu-system-x86_64 -enable-kvm -m 16G -cdrom out/$(shell ls -t out| head -n1) -boot menu=on \
	-drive file=test/disk_im.qcow2,format=qcow2,if=none,id=nvm -device nvme,serial=deadbeef,drive=nvm

run-uefi: test/disk_im.qcow2
	qemu-system-x86_64 -enable-kvm -m 16G -cdrom  out/$(shell ls -t out| head -n1)  -boot menu=on \
	-drive file=test/disk_im.qcow2,format=qcow2,if=none,id=nvm -device nvme,serial=deadbeef,drive=nvm \
	-bios /usr/share/ovmf/x64/OVMF.fd

test/disk_im.qcow2:
	qemu-img create -f qcow2 test/disk_im.qcow2  40G

clean:
	sudo rm -f out/* test/*
