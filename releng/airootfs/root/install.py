#!/usr/bin/python

# NikOS installation script

# A carefully curated list of bespoke packages 
packages = ("base base-devel linux linux-firmware alacritty arandr archey3 archiso autoconf automake bash bat bc " 
            "bdf-unifont binutils bison blender btrfs-progs bzip2 cmake coreutils cpupower cups cups-pdf ddcutil dhcpcd dialog "
            "discord dmenu dosfstools e2fsprogs edk2-ovmf emacs evince fakeroot feh file filesystem findutils "
            "firefox flex gawk gcc gcc-libs gettext gimp git glibc go go-tools gopls grep groff grub gvfs gvfs-mtp gvfs-smb "
            "gzip htop hunspell-en_us i3-gaps i3blocks i3lock i3status inetutils iproute2 iputils jp2a libc++ libc++abi "
            "libreoffice-fresh libtool libvncserver licenses linux-headers lvm2 m4 make man-db man-pages mesa minitube nano "
            "neofetch newsboat ninja nmap noto-fonts noto-fonts-cjk nss-mdns ntfs-3g openssh pacman patch pavucontrol "
            "pciutils phonon-qt5-vlc picom pkgconf procps-ng psmisc pulseaudio pulseaudio-alsa pyside2 python python-matplotlib "
            "qemu remmina sane-airscan scrot sed shadow simple-scan simplescreenrecorder sudo system-config-printer systemd "
            "systemd-sysvcompat tar texinfo texlive-core transmission-cli ttf-dejavu ttf-nerd-fonts-symbols-mono unrar "
            "unzip util-linux vlc wget which whois xdg-user-dirs  xorg-bdftopcf xorg-iceauth xorg-mkfontscale "
            "xorg-server xorg-sessreg xorg-setxkbmap xorg-smproxy xorg-x11perf xorg-xauth xorg-xbacklight xorg-xcmsdb "
            "xorg-xcursorgen xorg-xdpyinfo xorg-xdriinfo xorg-xev xorg-xgamma xorg-xhost xorg-xinit xorg-xinput xorg-xkbcomp "
            "xorg-xkbevd xorg-xkbutils xorg-xkill xorg-xlsatoms xorg-xlsclients xorg-xmodmap xorg-xpr xorg-xprop xorg-xrandr "
            "xorg-xrdb xorg-xrefresh xorg-xset xorg-xsetroot xorg-xvinfo xorg-xwd xorg-xwininfo xorg-xwud xterm xz youtube-dl "
            "zip zsh zsh-autosuggestions zsh-syntax-highlighting zsh-theme-powerlevel10k  "
            " efibootmgr ")


import subprocess, os, sys, time

def cmd(cmd):
    print(cmd)
    return subprocess.run(cmd, shell=True)

def printFancy(text):
    print('')
    print('='*(len(text)+4))
    print(f"= {text} =")
    print('='*(len(text)+4))
    print('')

def banner(text):
    cmd(f"figlet -c -f slant {text}")


time.sleep(2)
banner("Welcome to NikOS")
print("\nNikOS is an install script for Arch masquerading as an operating system (how self-aggrandizing!)")
print("\n\nYou can run the script as is or stop here and review what the script does (by typing `nano install.py.`). ")
print("Resume the installation with `./install.py`.")
stop = ''
while stop != 'y' and stop != 'n':
    stop = input('\nStop? (y/n):')
if stop == 'y':
    sys.exit()
    

# Check if running in UEFI mode
try:
    files = os.listdir('/sys/firmware/efi/efivars')
    if len(files) > 0:
        print("Running in UEFI mode.")
        bootMode = 'gpt'
    else:
        print("Running in BIOS mode.")
        bootMode = "msdos"
except:
    bootMode = "msdos"
    print(f'No directory /sys/firmware/efi/efivars. Running in BIOS mode.')

    
# Check networking
print("Pinging to check internet connection...")
res = cmd('ping -c 1 archlinux.org')
if res.returncode != 0:
    print("No internet connection... Exiting to shell so you can fix that. If on wireless, type `iwctl` and connect to an access point.")
    print("Resume the installation with ./install.py ")
    sys.exit()

    
# make sure the clock is accurate
cmd('timedatectl set-ntp true')


# Select drive to install to
output = subprocess.run('fdisk -l  | grep "Disk /" | cut -f2 -d" " | tr -d ":"', shell=True, capture_output=True, text=True)
disks = output.stdout.split()
printFancy("Choose disk to install nikOS on - ALL CONTENTS WILL BE DELETED")
diskNo = 0
for i in disks:
    print(f'{diskNo}: {disks[diskNo]}')
    diskNo += 1
choice = 0
done = False
while not done: 
    try:
        choice = int(input(f"Enter 0-{diskNo-1}: "))
    except:
        choice = -1
    if choice >= 0 and choice < diskNo:
        done = True
print(f"Installing to {disks[choice]}")


# Partition and format
print("\nPartitioning...")
if bootMode == 'gpt':
    cmd(f'parted --script {disks[choice]} mklabel {bootMode} mkpart EFI fat32 1MiB 261MiB set 1 esp on mkpart swap linux-swap 261MiB 4GiB mkpart root  ext4 4GiB 100\%')
else:
    cmd(f'parted --script {disks[choice]} mklabel {bootMode} mkpart primary linux-swap 1MiB 4GiB mkpart primary ext4 4GiB 95\% set 2 boot on')
    
cmd(f'parted --script {disks[choice]} print')

efiPart = ""
if "nvme" in disks[choice]:
    if bootMode == 'gpt':
        rootPart = disks[choice] + "p3"
        swapPart = disks[choice] + "p2"
        efiPart = disks[choice] + "p1"
    else:
        rootPart = disks[choice] + "p2"
        swapPart = disks[choice] + "p1"
else:
    if bootMode == 'gpt':
        rootPart = disks[choice] + "3"
        swapPart = disks[choice] + "2"
        efiPart = disks[choice] + "1"
    else:
        rootPart = disks[choice] + "2"
        swapPart = disks[choice] + "1"
        

print(f"Formating root partion {rootPart}")
cmd(f'mkfs.ext4 {rootPart}')
cmd(f'mkswap {swapPart}')
cmd(f"swapon {swapPart}")
if bootMode == 'gpt':
    cmd(f"mkfs.fat -F32 {efiPart}")
cmd(f"mount {rootPart} /mnt")
cmd('mkdir /mnt/etc')
cmd('genfstab -U /mnt > /mnt/etc/fstab')
print("/etc/fstab:")
cmd('cat /mnt/etc/fstab')


# Install packages

# look for microcode
intelMicrocode = ""
output = subprocess.run('cat /proc/cpuinfo | grep -i intel', shell=True, capture_output=True, text=True)
if output.stdout:
    intelMicrocode = "intel-ucode"
amdlMicrocode = ""
output = subprocess.run('cat /proc/cpuinfo | grep -i amd', shell=True, capture_output=True, text=True)
if output.stdout:
    amdMicrocode = "amd-ucode"

# GPU drivers
printFancy("Scanning GPU")
output = subprocess.run('lspci | grep VGA', shell=True, capture_output=True, text=True)
print(output.stdout)
print("Enter driver name. This is typically xf86-video-amdgpu, xf86-video-intel or nvidia.")
print("See https://wiki.archlinux.org/title/Xorg if in doubt.")
gpuDriver = input("Driver name:")

# wireless
iwd=''
while iwd != 'y' and iwd != 'n':
    iwd = input('Install wireless service iwd (y/n):')
if iwd == 'y':
    iwd = ' iwd '
if iwd == 'n':
    iwd = ''
   

printFancy("Installing packages")
cmd(f"pacstrap /mnt {packages} {amdlMicrocode} {intelMicrocode} {gpuDriver} {iwd}")


# Copy custom configs
cmd("cp -r ./configs/dotfiles /mnt/")
cmd("cp ./configs/locale.gen ./configs/locale.conf /mnt/etc/")
cmd("cp ./config.py /mnt/")
cmd("chmod a+x /mnt/config.py")


# Chroot and resume installation there
printFancy("Chroot into new system")
cmd(f"arch-chroot /mnt /config.py {bootMode} {disks[choice]} {efiPart}")

