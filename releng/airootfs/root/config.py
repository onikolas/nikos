#!/usr/bin/python

# NikOS configuration script. This is called automatically from ./install.py


import sys, subprocess
bootMode = sys.argv[1]
disk = sys.argv[2]
if len(sys.argv)>3:
    efiPart =  sys.argv[3]


def cmd(cmd):
    print(cmd)
    return subprocess.run(cmd, shell=True)


# locale
print("Setting locale")
cmd("ln -sf /usr/share/zoneinfo/Asia/Nicosia /etc/localtime")
cmd("locale-gen")
cmd("hwclock --systohc")

# enable networking
cmd('systemctl enable dhcpcd.service')
cmd('systemctl enable iwd.service')

# grub
print("Installing grub")
if bootMode == 'gpt':
    cmd("mkdir /efi")
    cmd(f"mount {efiPart} /efi")
    cmd("grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB")
else:
    cmd(f"grub-install --target=i386-pc {disk}")
cmd("grub-mkconfig -o /boot/grub/grub.cfg")

# Users
print("Set root password")
cmd("passwd") # set root passwd
print("Setup default user")
username = input("Enter your username:")
cmd(f"useradd -m -s /bin/zsh {username}")
cmd(f"passwd {username}")

# Install custom configs
userHome = f'/home/{username}'
cmd(f"cp -r /dotfiles {userHome}")
dotDir = f'/home/{username}/dotfiles'

cmd(f"rm -f {userHome}/.emacs {userHome}/.config/i3/config {userHome}/.Xresources ")
cmd(f"rm -f {userHome}/.Xmodmap {userHome}/.config/i3status/config ")
cmd(f"rm -f {userHome}/.zshrc  {userHome}/.zprofile  {userHome}/.config/picom.conf  {userHome}/.config/alacritty/alacritty.yml")

cmd(f"mkdir -p {userHome}/.config/i3/ {userHome}/.config/i3status/ {userHome}/.config/alacritty/ ")

cmd(f"ln -s  {dotDir}/emacs  {userHome}/.emacs")
cmd(f"ln -s  {dotDir}/i3-config  {userHome}/.config/i3/config")
cmd(f"ln -s  {dotDir}/Xresources {userHome}/.Xresources ")
cmd(f"ln -s  {dotDir}/Xmodmap {userHome}/.Xmodmap")
cmd(f"ln -s  {dotDir}/i3status {userHome}/.config/i3status/config")
cmd(f"ln -s  {dotDir}/zshrc {userHome}/.zshrc ")
cmd(f"ln -s  {dotDir}/zprofile {userHome}/.zprofile")
cmd(f"ln -s  {dotDir}/picom.conf {userHome}/.config/picom.conf  ")
cmd(f"ln -s  {dotDir}/alacritty.yml {userHome}/.config/alacritty/alacritty.yml")
cmd(f'echo "exec i3" >> {userHome}/.xinitrc')

cmd(f"chown -R {username}:{username} {userHome}")

# Done!
print("\n Done! Please `reboot`")



