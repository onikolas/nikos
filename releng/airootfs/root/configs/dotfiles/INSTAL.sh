# Install configuration files; execute with care

CURRENT=`dirname $(readlink -f $0)`

rm  ~/.emacs ~/.config/i3/config  ~/.Xresources ~/.Xmodmap  ~/.config/i3status/config
rm  ~/.zshrc  ~/.zprofile  ~/.config/picom.conf  ~/.config/alacritty/alacritty.yml
rm  ~/.newsboat/config ~/.newsboat/urls

ln -s  $CURRENT/emacs  ~/.emacs
ln -s  $CURRENT/i3-config  ~/.config/i3/config
ln -s  $CURRENT/Xresources ~/.Xresources 
ln -s  $CURRENT/Xmodmap ~/.Xmodmap
ln -s  $CURRENT/i3status ~/.config/i3status/config
ln -s  $CURRENT/zshrc ~/.zshrc 
ln -s  $CURRENT/zprofile ~/.zprofile
ln -s  $CURRENT/picom.conf ~/.config/picom.conf  
ln -s  $CURRENT/alacritty.yml ~/.config/alacritty/alacritty.yml
ln -s  $CURRENT/newsboat-config ~/.newsboat/config

