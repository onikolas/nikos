;; Don't polute with custom stuff
(setq custom-file "~/.emacs.d/emacs-custom.el")
     (load custom-file)

;; package
(setq package-archives '(("melpa" . "http://melpa.org/packages/")
                         ("gnu" . "http://elpa.gnu.org/packages/")))
(require 'package)
(package-initialize)

;; make sure use-package is there
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Appearance
(add-to-list 'default-frame-alist '(font . "Noto Sans Mono-12" ))
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'doom-nord  t)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-startup-screen t) 
(setq default-tab-width 4)
;;(desktop-save-mode 1)
(setq scroll-preserve-screen-position t)
(setq-default fill-column 100)

;; CC modes style and tab width
(setq c-default-style "linux"
      c-basic-offset 4
      tab-width 4)
(electric-pair-mode 1)
(show-paren-mode 1)

;; CUA mode
(cua-mode t)
(setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy t) ;; Standard Windows behaviour


(use-package helm
  :demand t
  :bind(("M-x"     . helm-M-x)
	("C-x r b" . helm-filtered-bookmarks)
	("C-x C-f" . helm-find-files)
	("<f6>"    . helm-find-files)
	("<f7>"    . helm-buffers-list))
  :commands (helm-mode)
  :config (require 'helm-config))


;; enable lsp
(use-package lsp-mode
  :ensure t
  :commands (lsp lsp-deferred)
  :hook (go-mode . lsp-deferred)
  :config
  (setq lsp-go-hover-kind "SynopsisDocumentation")
  (setq lsp-completion-mode 1))

;; Set up before-save hooks to format buffer and add/delete imports.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; Optional - provides fancier overlays.
;;(use-package lsp-ui
;;  :ensure t
;;  :commands lsp-ui-mode)

;; Company mode is a standard completion package that works well with lsp-mode.
(use-package company
  :ensure t
  :config
  ;; Optionally enable completion-as-you-type behavior.
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 1)  )

;; Optional - provides snippet support.
;;(use-package yasnippet
;;  :ensure t
;;  :commands yas-minor-mode
;;  :hook (go-mode . yas-minor-mode))

;; Go mode tabs
(add-hook 'go-mode-hook
	  (lambda ()
	    (setq-default)
	    (setq tab-width 4)
	    (setq standard-indent 4)))

;; Python
(use-package elpy
  :ensure t
  :init
  (elpy-enable))

;; Global custom shortcuts
(global-set-key (kbd "C-.") 'other-window)
(global-set-key (kbd "C-,") 'prev-window)

(defun prev-window ()
  (interactive)
  (other-window -1))


(defun kill-other-buffers ()
  "Kill all other buffers."
  (interactive)
  (mapc 'kill-buffer (delq (current-buffer) (buffer-list))))


(global-set-key (kbd "C-x g") 'magit-status)


;; Compilation (F5 behaviour)
(defun my-compile ()
  "Run compile and resize the compile window"
  (interactive)
  (progn
    (call-interactively 'compile)
    (setq cur (selected-window))
    (setq w (get-buffer-window "*compilation*"))
    (select-window w)
    (setq h (window-height w))
    (shrink-window (- h 10))
    (select-window cur)
    )
  )

(defun my-compilation-hook () 
  "Make sure that the compile window is splitting vertically"
  (progn
    (if (and (not (get-buffer-window "*compilation*")) (> (length (window-list)) 1) )
        (progn
	  (split-window-vertically)
	  )
      )
    )
  )

(defun my-kill-compilation ()
  (interactive)
  (progn
    (if (get-buffer "*compilation*") ; If old compile window exists
  	(progn
  	  (delete-windows-on (get-buffer "*compilation*")) ; Delete the compilation windows
  	  (kill-buffer "*compilation*") ; and kill the buffers
  	  )
      )
    )
  )

(add-hook 'compilation-mode-hook 'my-compilation-hook)
(global-set-key [f5] 'my-compile)
(global-set-key (kbd "C-<f5>")  'my-kill-compilation)


(use-package rustic
  :ensure
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status))
  :config
  ;; uncomment for less flashiness
  ;; (setq lsp-eldoc-hook nil)
  ;; (setq lsp-enable-symbol-highlighting nil)
  ;; (setq lsp-signature-auto-activate nil)

  ;; comment to disable rustfmt on save
  (setq rustic-format-on-save t)
  (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))
